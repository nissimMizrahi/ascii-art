extern crate image;
extern crate argparse;

use std::collections::BTreeMap;

use image::{
    GenericImageView, 
    DynamicImage, 
    ImageBuffer,
    Rgba
    };

use argparse::{
    ArgumentParser, 
    StoreTrue, 
    Store
    };

use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;
use std::io::prelude::*;

fn get_vecs(pic: &DynamicImage, invert: bool) -> Vec<Vec<u8>>
{
    let (width, height) = pic.dimensions();
    let mut buffer = vec![vec![0; width as usize]; height as usize];

    for (x, y, pixel) in pic.pixels() {
        let val = if invert {255 - pixel[0] as u8} 
        else {pixel[0] as u8};
        buffer[y as usize][x as usize] = val;
    }
    return buffer;
}

fn get_image_buffer(path: &str, width: u32, invert: bool) -> Result<Vec<Vec<u8>>, String>
{
    let img = match image::open(path)
    {
        Ok(image) => image,
        Err(e) => return Err(e.to_string()),
    };

    let (i_width, i_height) = img.dimensions();

    let mut grayed = img.grayscale();
    
    let height = (width as f32 * (i_height as f32 / i_width as f32)) as u32;

    grayed = grayed.resize(width, height, image::imageops::Gaussian);

    return Ok(get_vecs(&grayed, invert));
}

fn map_num(val: i32, min: i32, max: i32, new_min: i32, new_max: i32) -> i32
{
    let mark: f32 = (val - min) as f32 / (max - min) as f32;
    return new_min + (mark * (new_max - new_min) as f32) as i32;
}

fn map_bytes(buff: &mut Vec<Vec<u8>>, min: i32, max: i32)
{
    for row in buff.iter_mut()
    {
        for val in row.iter_mut()
        {
            *val = map_num(*val as i32, 0, 255, min, max) as u8;
        }
    } 
}

fn get_weights(path: &str) -> Result<BTreeMap<u8, char>, String>
{
    let f = match File::open(path) 
    {
            Ok(file) => io::BufReader::new(file),
            Err(e) => return Err(format!("{}: {}", e.to_string(), path)),
    };

    let mut ret = BTreeMap::new();
    
    for (i, line) in f.lines().enumerate()
    {
        let mut line = line.unwrap();

        let val: char = match FromStr::from_str(&line)
        {
            Ok(l) => l,
            Err(_e) => return Err(format!("{} is not in format:\n<letter>\n<letter>\n...", path)),
        };
        ret.insert(i as u8 ,val);

    }
    return Ok(ret);
}

fn parse_args(width: &mut u32, weights: &mut String, input: &mut String, invert: &mut bool)
{
    let mut ap = ArgumentParser::new();
    ap.set_description("turn an image to ascii art");
    
    ap.refer(input).required()
        .add_argument("input", Store,
        "the input file");

    ap.refer(width)
        .add_option(&["-w", "--width"], Store,
        "width of the new image (in letters)");

    ap.refer(weights)
        .add_option(&["-W", "--weights"], Store,
        "file who stores the letters weights, each line is a letter and the lower the letter is the higher the value");
        
    ap.refer(invert)
        .add_option(&["-i", "--invert"], Store,
        "inverts the color of the image for some interesting results");
    ap.parse_args_or_exit();
}

fn main() 
{

    let mut width: u32 = 700;
    let mut weights_file = String::from("weights.csv");
    let mut input = String::from("");
    let mut invert_color = false;

    parse_args(&mut width, &mut weights_file, &mut input, &mut invert_color);

    let mut img = match get_image_buffer(&input, width, invert_color)
    {
        Ok(i) => i,
        Err(e) =>
        {
            println!("cant proceed operation:\n{}", e);
            return;
        },
    };

    let weights = match get_weights(&weights_file)
    {
        Ok(w) => w,
        Err(e) =>
        {
            println!("cant proceed operation:\n{}", e);
            return;
        },
    };

    map_bytes(&mut img, 0, (weights.len() - 1) as i32);

    let input_parts: Vec<&str> = input.split(".").collect();
    let output = format!("{}_ascii_{}_{}.txt", input_parts[0], width, if invert_color {"inverted"} else {"not_inverted"});
    let mut buffer = File::create(output).unwrap();

    for row in img.iter_mut()
    {
        for val in row.iter_mut()
        {
            let letter: char = weights[val];

            buffer.write(&[letter as u8]);
        }
        writeln!(&mut buffer, "").unwrap();
    } 

}
